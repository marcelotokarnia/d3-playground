# D3 Playground

## How to hack locally

TL;DR:

* `npm i`

* `npm run watch`

Longer version:

#### Frontend requirements

You will need [npm](https://www.npmjs.com/) and [Node](https://nodejs.org).

On Linux you might get those by `sudo apt install npm` and `sudo apt install nodejs`

Then install project requirements locally:

`npm i`

#### Watch over your changes

If you have a terminal tab running

`npm run watch`

you can change your files and the webpack will compile than instantly, open your `public/index.html` file and after each page refresh you will see your latest changes rendered on the browser.

## About the project

This is a sample of D3.js Line graph, fetching points from API, and adding new points to graph with clicking.

The frontend components were developed using React and Typescript.

Author: [Marcelo Tokarnia](https://www.github.com/marcelotokarnia)

Deployed at: [d3-tokarnia@now](https://d3-tokarnia.now.sh/)
