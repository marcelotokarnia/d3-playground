import { IResponse, IValue } from '@typings/globals'
import axios, { AxiosPromise } from 'axios'

const getValues = (): AxiosPromise<{values: IValue[]}> =>
  axios.get<{values: IValue[]}>('https://konuxdata.getsandbox.com/data')

const saveValue = (value: IValue): AxiosPromise<IResponse> =>
  axios.post<IResponse>('https://konuxdata.getsandbox.com/points', value)

export {
  getValues,
  saveValue,
}
