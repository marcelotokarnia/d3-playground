import Chart from '@components/Chart'
import { getValues, saveValue } from '@src/api'
import { IGraphValue, IValue } from '@typings/globals'
import * as R from 'ramda'
import * as React from 'react'

interface IState {
  loading: boolean
  message: string
  values: IValue[]
}

class App extends React.Component {
  state: IState = {
    loading: false,
    message: '',
    values: [],
  }

  componentDidMount() {
    this.setState({
      loading: true,
    }, () => {
      getValues()
        .then(({ data: { values } }: { data: {values: IValue[]} }): void => {
          this.setState({
            values,
          })
        })
        .catch((e: any): void => {
          return
        })
        .then((): void => {
          this.setState({
            loading: false,
          })
        })
    })
  }

  sendValue = ({x, y}: IValue): void => {
    saveValue({
      x,
      y,
    }).then(({data: {status}}) => {
      if (R.equals('ok', status)) {
        this.setState({
          loading: true,
        }, () => this.setState({
          loading: false,
          message: <p className="pt-70">
            Added new point to graph{' '}
            <span className="blue">
              ({x},
            </span>
            <span className="red">
               {' '}{y})
            </span>
          </p>,
          values: R.append({x, y}, this.state.values),
        }))
      }
    })
  }

  render() {
    const { loading, message, values } = this.state
    return !loading && (
      <div className="center">
        <Chart data={values} id="d3svg" onClick={this.sendValue}/>
        <span>{ message }</span>
      </div>
    )
  }

}

export default App
