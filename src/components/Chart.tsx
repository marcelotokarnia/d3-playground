import { createChart } from '@src/utils/charts'
import { IGraphValue, IValue } from '@typings/globals'
import * as PropTypes from 'prop-types'
import * as React from 'react'

interface IChartProps {
  data: IValue[]
  id: string
  onClick: (value: IValue) => void
}

class Chart extends React.Component<IChartProps> {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape({
      x: PropTypes.string,
      y: PropTypes.number,
    })),
    id: PropTypes.string,
    onClick: PropTypes.func,
  }

  componentDidMount() {
    const { id, data, onClick } = this.props
    if (data.length) {
      const svg = createChart(data, id, onClick)
    }
  }

  render() {
    const { id } = this.props
    return <svg
      id={id}
      width="960"
      height="500"
      className="pt-70 pl-70"
    ></svg>
  }
}

export default Chart
