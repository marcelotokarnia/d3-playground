import * as React from 'react'
import * as ReactDOM from 'react-dom'

import App from '@src/components/App'
import './styles.styl'

ReactDOM.render(<App />, window.document.getElementById('app'))

export default App
