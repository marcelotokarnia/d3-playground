import { IGraphValue, IValue } from '@typings/globals'
import { extent } from 'd3-array'
import { axisBottom, axisLeft } from 'd3-axis'
import { scaleLinear, scaleTime } from 'd3-scale'
import { mouse, select } from 'd3-selection'
import { line } from 'd3-shape'
import { timeFormat } from 'd3-time-format'
import * as moment from 'moment'
import * as R from 'ramda'

const createChart = (data: IValue[], id: string, onClick?: (value: IValue) => void): void => {
  const svg = select(`#${id}`)
  const left = 50
  const top = 20
  const right = 20
  const bottom = 30
  const margin = {top, right, bottom, left}
  const width = +svg.attr('width') - left - right
  const height = +svg.attr('height') - top - bottom
  const g = svg.append('g').attr('transform', `translate(${left},${top})`)

  const massagedData: IGraphValue[] = R.map<IValue, IGraphValue>(
    ({x, y}: IValue) => ({x: +moment(x), y}), data,
  )

  massagedData.sort(({x: xa}, {x: xb}) => (xa - xb) || 1)

  svg
   .append('svg')
     .attr('width', width + margin.left + margin.right)
     .attr('height', height + margin.top + margin.bottom)
   .append('g')
     .attr('transform', `translate(${margin.left}, ${margin.top})`)

  const xAxis = scaleTime()
    .domain(extent<IGraphValue, number>(massagedData, ({x}) => x) as [number, number])
    .range([0, width])
    .nice()

  const yAxis = scaleLinear()
    .domain(extent<IGraphValue, number>(massagedData, ({y}) => y) as [number, number])
    .range([height, 0])
    .nice()

  const chartLine = line<IGraphValue>()
    // @ts-ignore
    .x(R.compose(xAxis, R.prop('x'))).y(R.compose(yAxis, R.prop('y')))

  svg
    .selectAll('.line')
    .data([massagedData])
    .enter()
    .append('path')
    .attr('class', 'line')
    .attr('fill', 'none')
    .attr('stroke', 'steelblue')
    .attr('stroke-linejoin', 'round')
    .attr('stroke-linecap', 'round')
    .attr('stroke-width', 1.5)
    .attr('d', chartLine)

  svg
    .append('g')
      .attr('transform', `translate(0, ${height})`)
      // @ts-ignore
    .call(axisBottom(xAxis).tickFormat(timeFormat('%Y-%m-%dT%HH')))
    .selectAll('text')
      .style('text-anchor', 'end')
      .attr('dx', '-.8em')
      .attr('dy', '.15em')
      .attr('transform', 'rotate(-25)')
      .attr('class', 'fill-blue')

  svg
    .append('g')
    // @ts-ignore
    .call(axisLeft(yAxis))
    .selectAll('text')
      .attr('class', 'fill-red')

  if (onClick) {
    svg.on('click', function() {
      const coords = mouse(this as any)

      const newData = {
        x: moment(xAxis.invert(coords[0])).format(),
        y: +yAxis.invert(coords[1]).toFixed(2),
      }

      onClick(newData)
    })
  }
}

export {
  createChart,
}
