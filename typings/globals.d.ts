interface IValue {
  x: string
  y: number
}

interface IGraphValue {
  x: number
  y: number
}

interface IResponse {
  status: string
}

export {
  IValue,
  IResponse,
  IGraphValue
}